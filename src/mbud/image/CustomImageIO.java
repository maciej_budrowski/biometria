package mbud.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class CustomImageIO {

	public static BufferedImage loadImage(File file) throws CvException {
		BufferedImage image = loadJavaSupportedImage(file);
		if (image == null) {
			Mat mat = Imgcodecs.imread(file.getPath());
			// Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2R, 0);

			image = new BufferedImage(mat.width(), mat.height(), BufferedImage.TYPE_3BYTE_BGR);

			byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
			mat.get(0, 0, data);
		} else if (image.getType() != BufferedImage.TYPE_3BYTE_BGR) {
			return ImageUtil.convertToType(image, BufferedImage.TYPE_3BYTE_BGR);
		}
		return image;
	}

	private static BufferedImage loadJavaSupportedImage(File file) {
		try {
			return ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void saveImage(BufferedImage image, File file) {
		if (!saveJavaSupportedImage(image, file)) {
			image = ImageUtil.convertToType(image, BufferedImage.TYPE_3BYTE_BGR);
			Mat mat = new Mat(image.getHeight(), image.getWidth(), ImageUtil.bufferedImageTypeToMatType(image.getType()));
			byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
			mat.put(0, 0, pixels);
			Imgcodecs.imwrite(file.getPath(), mat);
		}
	}

	private static boolean saveJavaSupportedImage(BufferedImage image, File file) {
		String extension = file.getName().split("\\.(?=[^\\.]+$)")[1].toUpperCase();
		ImageWriter writer = null;
		ImageOutputStream stream = null;
		try {
			writer = ImageIO.getImageWritersByFormatName(extension).next();
			stream = ImageIO.createImageOutputStream(file);
			writer.setOutput(stream);
			ImageWriteParam params = writer.getDefaultWriteParam();
			params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			if (extension.equalsIgnoreCase("gif")) {
				params.setCompressionType("lzw");
			}
			params.setCompressionQuality(1);
			writer.write(null, new IIOImage(ImageUtil.bufferedImageToRendered(image), null, null), params);
			if (writer != null) {
				writer.abort();
				if (stream != null) {
					stream.close();
				}
				writer.dispose();
			}
			//return ImageIO.write(ImageUtil.bufferedImageToRendered(image), extension, file);
			return true;
		} catch (Exception e) {
			if (writer != null) {
				writer.abort();
				if (stream != null) {
					try {
						stream.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				writer.dispose();
			}
			return false;
		}
	}
}
