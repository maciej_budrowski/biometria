package mbud.image;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.RenderedImage;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class ImageUtil {
	private static final int INVALID_TYPE = -1;
	
	public static BufferedImage scaleImage(BufferedImage image, double scaleX, double scaleY) {
		return scaleImage(image, (int) (image.getWidth() * scaleX), (int) (image.getWidth() * scaleY));
	}

	public static BufferedImage scaleImage(BufferedImage image, int targetWidth, int targetHeight) {
		BufferedImage newImage = new BufferedImage(targetWidth, targetHeight, image.getType());
		
		Graphics2D g2d = newImage.createGraphics();
		g2d.drawImage(image, 0, 0, targetWidth, targetHeight, null);
		g2d.dispose();
		
		return newImage;
	}
	
	public static RenderedImage bufferedImageToRendered(BufferedImage image) {
		try {
			return (RenderedImage) image;
		} catch (ClassCastException ex) {
			Graphics2D graphics = image.createGraphics();
			graphics.drawImage(image, null, null);
			return (RenderedImage) image;
		}
	}

	public static BufferedImage convertToType(BufferedImage original, int type) {
		if (original == null) {
			return null;
		}

		if (original.getType() == type) {
			return original;
		}
		
		return copyOfType(original, type);
	}
	
	public static BufferedImage copyOfType(BufferedImage original, int type) {
		if (original == null) {
			return null;
		}
		
		BufferedImage image = new BufferedImage(original.getWidth(), original.getHeight(), type);

		Graphics2D g2d = image.createGraphics();
		try {
			g2d.setComposite(AlphaComposite.Src);
			g2d.drawImage(original, 0, 0, null);
		} finally {
			g2d.dispose();
		}

		return image;
	}
	
	public static BufferedImage copy(BufferedImage original) {
		return copyOfType(original, original.getType());
	}

	static int bufferedImageTypeToMatType(int bufferedImageType) {
		switch (bufferedImageType) {
		case BufferedImage.TYPE_3BYTE_BGR:
			return CvType.CV_8UC3;
		case BufferedImage.TYPE_BYTE_GRAY:
		case BufferedImage.TYPE_BYTE_BINARY:
			return CvType.CV_8UC1;
		case BufferedImage.TYPE_INT_BGR:
		case BufferedImage.TYPE_INT_RGB:
			return CvType.CV_32SC3;
		case BufferedImage.TYPE_INT_ARGB:
		case BufferedImage.TYPE_INT_ARGB_PRE:
			return CvType.CV_32SC4;
		case BufferedImage.TYPE_USHORT_GRAY:
			return CvType.CV_16UC1;
		case BufferedImage.TYPE_4BYTE_ABGR:
		case BufferedImage.TYPE_4BYTE_ABGR_PRE:
			return CvType.CV_8UC4;
		}
		return INVALID_TYPE;
	}

	static int matTypeToImageType(int matType) {
		if (matType == CvType.CV_8UC3) {
			return BufferedImage.TYPE_3BYTE_BGR;
		}
		else if (matType == CvType.CV_8UC1) {
			return BufferedImage.TYPE_BYTE_GRAY;
		}
		else if (matType == CvType.CV_32SC3) {
			return BufferedImage.TYPE_INT_RGB;
		}
		else if (matType == CvType.CV_32SC4) {
			return BufferedImage.TYPE_INT_ARGB;
		}
		else if (matType == CvType.CV_16UC1) {
			return BufferedImage.TYPE_USHORT_GRAY;
		}
		else if (matType == CvType.CV_8UC4) {
			return BufferedImage.TYPE_4BYTE_ABGR;
		}
		return INVALID_TYPE;
	}
	
	public static Mat imageToMat(BufferedImage image) {
		Mat mat = new Mat(image.getHeight(), image.getWidth(), bufferedImageTypeToMatType(image.getType()));
		byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		mat.put(0, 0, pixels);
		return mat;
	}
	
	public static BufferedImage matToImage(Mat mat) {
		BufferedImage image = new BufferedImage(mat.cols(), mat.rows(), matTypeToImageType(mat.type()));
		byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		mat.get(0, 0, pixels);
		return image;
	}
}
