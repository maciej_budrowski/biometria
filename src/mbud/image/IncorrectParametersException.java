package mbud.image;

public class IncorrectParametersException extends Exception {
	private static final long serialVersionUID = -5104899411751824656L;
	
	private String parName, parValue;
	
	public IncorrectParametersException(String parameter, String value) {
		this.parName = parameter;
		this.parValue = value;
	}
	
	@Override
	public String getMessage() {
		return "Parameter \"" + parName + "\" cannot accept value \"" + parValue + "\"!";
	}
}
