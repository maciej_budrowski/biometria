package mbud.image;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import mbud.util.MathUtil;

public class ImageProcessor {

	public enum Histogram {
		GRAY(0), RED(1), GREEN(2), BLUE(3);

		private int val;

		private Histogram(int val) {
			this.val = val;
		}

		public int getIndex() {
			return val;
		}
	}

	public ImageProcessor() {

	}

	// Funkcja tworząca histogramy (szary, czerwony, zielony, niebieski) o
	// określonym rozmiarze dla podanego obrazu
	public BufferedImage[] generateHistogram(BufferedImage image, Dimension histogramSize) {
		// Konwersja obrazu na macierz
		Mat mat = ImageUtil.imageToMat(image);
		// Zmiana kolorów z BGR na RGB
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGB);

		// Rozbicie macierzy obrazu na macierze poszczególnych kanałów
		// (czerwony, zielony, niebieski)
		List<Mat> planes = new ArrayList<Mat>();
		Core.split(mat, planes);

		// Utworzenie macierz uśrednionej ((R+G+B)/3)
		Mat rgbMat = new Mat(planes.get(0).rows(), planes.get(1).cols(), CvType.CV_8U);
		byte[] tmp = new byte[1];
		int val;
		for (int r = 0; r < planes.get(0).rows(); r++) {
			for (int c = 0; c < planes.get(0).cols(); c++) {
				val = 0;
				for (int i = 0; i < planes.size(); i++) {
					planes.get(i).get(r, c, tmp);
					val += (int) (tmp[0] & 0xFF);
				}
				val /= planes.size();
				tmp[0] = (byte) val;
				rgbMat.put(r, c, tmp);
			}
		}

		// Utworzenie macierzy na histogramy
		Mat rgbHist = new Mat();
		Mat rHist = new Mat();
		Mat gHist = new Mat();
		Mat bHist = new Mat();

		// Dostosowanie macierzy do wymagań funkcji calcHist
		List<Mat> rgbList = new LinkedList<>();
		rgbList.add(rgbMat);
		List<Mat> rList = new LinkedList<>();
		rList.add(planes.get(0));
		List<Mat> gList = new LinkedList<>();
		gList.add(planes.get(1));
		List<Mat> bList = new LinkedList<>();
		bList.add(planes.get(2));

		// Ustalenie parametrów funkcji calcHist
		MatOfInt histSize = new MatOfInt(256);
		MatOfFloat histRange = new MatOfFloat(0f, 256f);
		boolean accumulate = false;

		// Tworzenie histogramów przy użyciu funkcji calcHist
		Imgproc.calcHist(rgbList, new MatOfInt(0), new Mat(), rgbHist, histSize, histRange, accumulate);
		Imgproc.calcHist(rList, new MatOfInt(0), new Mat(), rHist, histSize, histRange, accumulate);
		Imgproc.calcHist(gList, new MatOfInt(0), new Mat(), gHist, histSize, histRange, accumulate);
		Imgproc.calcHist(bList, new MatOfInt(0), new Mat(), bHist, histSize, histRange, accumulate);

		// Przygotowanie wymiarów histogramu, a także grubości każdego słupka
		int histWidth = (int) histogramSize.getWidth();
		int histHeight = (int) histogramSize.getHeight();
		int barWidth = (int) Math.round(histWidth / 256.0);

		// Normalizacja histogramu do zakresu 1-docelowa wysokość histogramu
		Core.normalize(rgbHist, rgbHist, 1, histHeight, Core.NORM_MINMAX);
		Core.normalize(rHist, rHist, 1, histHeight, Core.NORM_MINMAX);
		Core.normalize(gHist, gHist, 1, histHeight, Core.NORM_MINMAX);
		Core.normalize(bHist, bHist, 1, histHeight, Core.NORM_MINMAX);

		// Utworzenie obrazów na graficzną reprezentację histogramów (każdy z
		// obrazów będzie miał 5 pikselowy margines)
		BufferedImage[] images = new BufferedImage[4];
		for (int i = 0; i < images.length; i++) {
			images[i] = new BufferedImage(histWidth + 10, histHeight + 10, BufferedImage.TYPE_3BYTE_BGR);
		}

		// Utworzenie obiektów grafik obrazów i ustawienie odpowiednich kolorów
		// i przesunięcia
		Graphics2D rgbGraphics = images[Histogram.GRAY.getIndex()].createGraphics();
		rgbGraphics.setColor(Color.WHITE);
		rgbGraphics.translate(5, 5);
		Graphics2D rGraphics = images[Histogram.RED.getIndex()].createGraphics();
		rGraphics.setColor(Color.RED);
		rGraphics.translate(5, 5);
		Graphics2D gGraphics = images[Histogram.GREEN.getIndex()].createGraphics();
		gGraphics.setColor(Color.GREEN);
		gGraphics.translate(5, 5);
		Graphics2D bGraphics = images[Histogram.BLUE.getIndex()].createGraphics();
		bGraphics.setColor(Color.BLUE);
		bGraphics.translate(5, 5);

		// Rysowanie słupków na obrazach histogramów
		int tempX, tempY;
		for (int i = 0; i < 256; i++) {
			tempX = barWidth * i;

			tempY = (int) (histHeight - Math.round(rgbHist.get(i, 0)[0]));
			for (int j = 0; j < barWidth; j++) {
				rgbGraphics.drawLine(tempX + j, histHeight - 1, tempX + j, tempY);
			}

			tempY = (int) (histHeight - Math.round(rHist.get(i, 0)[0]));
			for (int j = 0; j < barWidth; j++) {
				rGraphics.drawLine(tempX + j, histHeight - 1, tempX + j, tempY);
			}

			tempY = (int) (histHeight - Math.round(gHist.get(i, 0)[0]));
			for (int j = 0; j < barWidth; j++) {
				gGraphics.drawLine(tempX + j, histHeight - 1, tempX + j, tempY);
			}

			tempY = (int) (histHeight - Math.round(bHist.get(i, 0)[0]));
			for (int j = 0; j < barWidth; j++) {
				bGraphics.drawLine(tempX + j, histHeight - 1, tempX + j, tempY);
			}
		}

		return images;
	}

	// Funkcja uśredniająca kolory obrazu
	public BufferedImage turnToGrayScale(BufferedImage image) {
		// Zamiana obrazu na macierz
		Mat mat = ImageUtil.imageToMat(image);

		// Utworzenie macierzy wyjściowej
		Mat dest = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC3);

		// Tymczasowe struktury danych do przetwarzania jasności warstw
		byte[] imageData = new byte[mat.channels()];
		byte[] destData = new byte[3];
		int acc;

		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				// Pobranie wartości obrazu zródłowego
				mat.get(r, c, imageData);

				// Uśrednienie wartości wszystkich kanałów
				acc = 0;
				for (int channel = 0; channel < mat.channels(); channel++) {
					acc += (int) (imageData[channel] & 0xFF);
				}
				acc /= mat.channels();

				// Wstawienie wartości do macierzy docelowej
				destData[0] = destData[1] = destData[2] = (byte) acc;
				dest.put(r, c, destData);
			}
		}

		// Konwersja macierz na obraz o odpowiednim typie
		return ImageUtil.convertToType(ImageUtil.matToImage(dest), BufferedImage.TYPE_3BYTE_BGR);
	}

	// Funkcja rozjaśniająca obraz przy użyciu funkcji wykładniczej
	public BufferedImage brighten(BufferedImage image, double k) {
		// Utworzenie lookup table operacji
		Mat lut = new Mat(1, 256, CvType.CV_8UC3);

		// Wypełnienie LUT korzystając ze wzoru 255 * (i/255)^1/k
		byte[] values = new byte[3];
		double exp = 1 / k;
		for (int i = 0; i < 256; i++) {
			values[0] = values[1] = values[2] = (byte) ((MathUtil.clamp(255.0 * Math.pow(i / 255.0, exp), 0.0, 255.0)
					.intValue()));
			lut.put(0, i, values);
		}

		// Konwersja obrazu na macierz
		Mat mat = ImageUtil.imageToMat(image);

		// Wykonanie operacji LUT
		Core.LUT(mat, lut, mat);

		// Konwersja macierzy na obraz
		return ImageUtil.matToImage(mat);
	}

	// Funkcja przyciemniająca obraz przy użyciu funkcji wykładniczej
	public BufferedImage darken(BufferedImage image, double k) {
		// Utworzenie lookup table operacji
		Mat lut = new Mat(1, 256, CvType.CV_8UC3);

		// Wypełnienie LUT korzystając ze wzoru 255 * (i/255)^k
		byte[] values = new byte[3];
		for (int i = 0; i < 256; i++) {
			values[0] = values[1] = values[2] = (byte) ((MathUtil.clamp(255.0 * Math.pow(i / 255.0, k), 0.0, 255.0)
					.intValue()));
			lut.put(0, i, values);
		}

		// Konwersja obrazu na macierz
		Mat mat = ImageUtil.imageToMat(image);

		// Wykonanie operacji LUT
		Core.LUT(mat, lut, mat);

		// Konwersja macierzy na obraz
		return ImageUtil.matToImage(mat);
	}

	// Funkcja rozciągająca histogram dla podanych wartości a i b
	public BufferedImage stretchHistogram(BufferedImage image, int a, int b) {
		// Utworzenie lookup table operacji
		Mat lut = new Mat(1, 256, CvType.CV_8UC3);

		// Wypełnienie LUT korzystając ze wzoru 255/(b-a) * (i - a)
		byte[] values = new byte[3];
		double step = 255.0 / (b - a);
		for (int i = 0; i < 256; i++) {
			values[0] = values[1] = values[2] = (byte) (MathUtil.clamp(step * (i - a), 0.0, 255.0).intValue());
			lut.put(0, i, values);
		}

		// Konwersja obrazu na macierz
		Mat mat = ImageUtil.imageToMat(image);

		// Wykonanie operacji LUT
		Core.LUT(mat, lut, mat);

		// Konwersja macierzy na obraz
		return ImageUtil.matToImage(mat);
	}

	// Funkcja wyrównująca histogram obrazu
	public BufferedImage equilizeHistogram(BufferedImage image) {
		Mat mat = ImageUtil.imageToMat(image);

		Mat ycrcb = new Mat();
		Imgproc.cvtColor(mat, ycrcb, Imgproc.COLOR_BGR2YCrCb);
		List<Mat> mats = new ArrayList<>(3);
		Core.split(ycrcb, mats);
		Imgproc.equalizeHist(mats.get(0), mats.get(0));
		Core.merge(mats, ycrcb);
		Mat dst = new Mat();
		Imgproc.cvtColor(ycrcb, dst, Imgproc.COLOR_YCrCb2BGR);

		return ImageUtil.matToImage(dst);
	}

	public BufferedImage binarize(BufferedImage image, int threshold) {
		Mat mat = ImageUtil.imageToMat(image);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(mat, mat, threshold, 255, Imgproc.THRESH_BINARY);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_GRAY2BGR);
		return ImageUtil.matToImage(mat);
	}

	public BufferedImage binarizeOtsu(BufferedImage image) {
		Mat mat = ImageUtil.imageToMat(image);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_GRAY2BGR);
		return ImageUtil.matToImage(mat);
	}

	// https://github.com/opencv/opencv_contrib/blob/246ea8f3bdf174a2aad6216c2601e3a93bf75c29/modules/ximgproc/src/niblack_thresholding.cpp
	public BufferedImage binarizeNiblack(BufferedImage image, int blockSize, float delta)
			throws IncorrectParametersException {
		if (blockSize % 2 != 1 || blockSize <= 1) {
			throw new IncorrectParametersException("blockSize", blockSize + "");
		}
		if (delta > 0) {
			throw new IncorrectParametersException("delta", delta + "");
		}
		Mat mat = ImageUtil.imageToMat(image);
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
		Mat dst = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
		Size size = mat.size();
		Mat mean = new Mat(size, CvType.CV_32F);
		Mat sqmean = new Mat(size, CvType.CV_32F);
		Mat stddev = new Mat(size, CvType.CV_32F);
		Mat thresh = new Mat(size, CvType.CV_32F);

		Imgproc.boxFilter(mat, mean, CvType.CV_32F, new Size(blockSize, blockSize), new Point(-1, -1), true,
				Core.BORDER_REPLICATE);
		//mean = pixelMean(mat, blockSize);
//		float[] a = new float[1];
//		for (int i = 0; i < 10; i++) {
//			for (int j = 0; j < 10; j++) {
//				mean.get(i, j, a);
//				System.out.println(i + ", " + j + ": " + a[0]);
//			}
//		}
		Imgproc.sqrBoxFilter(mat, sqmean, CvType.CV_32F, new Size(blockSize, blockSize), new Point(-1, -1), true,
				Core.BORDER_REPLICATE);
		//sqmean = pixelSqMean(mat, blockSize);
//		for (int i = 0; i < 10; i++) {
//			for (int j = 0; j < 10; j++) {
//				sqmean.get(i, j, a);
//				System.out.println(i + ", " + j + ": " + a[0]);
//			}
//		}
		Core.subtract(sqmean, mean.mul(mean), sqmean);
		Core.sqrt(sqmean, stddev);
		Core.multiply(stddev, new Scalar(delta), stddev);
		Core.add(mean, stddev, thresh);
		thresh.convertTo(thresh, CvType.CV_8UC1);

		Mat mask = new Mat();
		Core.compare(mat, thresh, mask, Core.CMP_GT);
		dst.setTo(new Scalar(0));
		dst.setTo(new Scalar(255), mask);

		Imgproc.cvtColor(dst, dst, Imgproc.COLOR_GRAY2BGR);
		return ImageUtil.matToImage(dst);
	}

	@SuppressWarnings("unused")
	private Mat pixelMean(Mat image, int blockSize) {
		Mat mean = new Mat(image.size(), CvType.CV_32F);

		byte[][] imageBytes = new byte[image.rows()][image.cols()];
		float[][] meanValues = new float[image.rows()][image.cols()];
		
		byte[] pixel = new byte[1];
		float[] meanFloat = new float[1];
		for (int r = 0; r < image.rows(); r++) {
			for (int c = 0; c < image.cols(); c++) {
				image.get(r, c, pixel);
				imageBytes[r][c] = pixel[0];
			}
		}

		int blockRadius = blockSize / 2;
		System.out.println(blockRadius);
		for (int r = -blockRadius; r < image.rows() + blockRadius; r++) {
			for (int c = -blockRadius; c < image.cols() + blockRadius; c++) {
				pixel[0] = imageBytes[(r < 0) ? 0 : (r >= image.rows() ? image.rows() - 1 : r)]
						[(c < 0) ? 0 : (c >= image.cols() ? image.cols() - 1 : c)];
				for (int i = r - blockRadius; i < r + blockRadius; i++) {
					for (int j = c - blockRadius; j < c + blockRadius; j++) {
						if (i < 0 || i >= image.rows() || j < 0 || j >= image.cols()) {
							continue;
						}
						meanValues[i][j] += (int) (pixel[0] & 0xFF);
					}
				}
			}
		}

		for (int r = 0; r < image.rows(); r++) {
			for (int c = 0; c < image.cols(); c++) {
				meanFloat[0] = meanValues[r][c] / (blockSize * blockSize);
				mean.put(r, c, meanFloat);
			}
		}
		return mean;
	}

	@SuppressWarnings("unused")
	private Mat pixelSqMean(Mat image, int blockSize) {
		Mat mean = new Mat(image.size(), CvType.CV_32F);

		byte[][] imageBytes = new byte[image.rows()][image.cols()];
		float[][] meanValues = new float[image.rows()][image.cols()];
		
		byte[] pixel = new byte[1];
		float[] meanFloat = new float[1];
		for (int r = 0; r < image.rows(); r++) {
			for (int c = 0; c < image.cols(); c++) {
				image.get(r, c, pixel);
				imageBytes[r][c] = pixel[0];
			}
		}

		int blockRadius = blockSize / 2;
		for (int r = -blockRadius; r < image.rows() + blockRadius; r++) {
			for (int c = -blockRadius; c < image.cols() + blockRadius; c++) {
				pixel[0] = imageBytes[(r < 0) ? 0 : (r >= image.rows() ? image.rows() - 1 : r)]
						[(c < 0) ? 0 : (c >= image.cols() ? image.cols() - 1 : c)];
				for (int i = r - blockRadius; i < r + blockRadius; i++) {
					for (int j = c - blockRadius; j < c + blockRadius; j++) {
						if (i < 0 || i >= image.rows() || j < 0 || j >= image.cols()) {
							continue;
						}
						meanValues[i][j] += ((int) (pixel[0] & 0xFF)) * ((int) (pixel[0] & 0xFF));
					}
				}
			}
		}

		for (int r = 0; r < image.rows(); r++) {
			for (int c = 0; c < image.cols(); c++) {
				meanFloat[0] = meanValues[r][c] / (blockSize * blockSize);
				mean.put(r, c, meanFloat);
			}
		}
		return mean;
	}
	
	public BufferedImage linearFilter(BufferedImage image, int[][] matrix) {
		int matrixWidth = matrix.length;
		int matrixHeight = matrix[0].length;
		int middleX = matrixWidth / 2;
		int middleY = matrixHeight / 2;
		float divider = 0f;
		for (int x = 0; x < matrixWidth; x++) {
			for (int y = 0; y < matrixHeight; y++) {
				divider += matrix[x][y];
			}
		}
		
		Mat mat = ImageUtil.imageToMat(image);

		byte[][][] matBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		byte[][][] targetBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		
		byte[] tmp = new byte[mat.channels()];
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				mat.get(r, c, tmp);
				for (int ch = 0; ch < mat.channels(); ch++) {
					matBytes[r][c][ch] = tmp[ch];
				}
			}
		}
		
		float sum;
		int deFactoRow, deFactoCol, offsetRow, offsetCol;
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					sum = 0;
					for (int x = 0; x < matrixWidth; x++) {
						for (int y = 0; y < matrixHeight; y++) {
							offsetRow = r + y - middleY;
							offsetCol = c + x - middleX;
							deFactoRow = (offsetRow < 0) ? 0 : (offsetRow >= mat.rows() ? mat.rows() - 1 : offsetRow);
							deFactoCol = (offsetCol < 0) ? 0 : (offsetCol >= mat.cols() ? mat.cols() - 1 : offsetCol);
							sum += matrix[x][y] * ((int) (matBytes[deFactoRow][deFactoCol][ch] & 0xFF));
						}
					}
					if (divider != 0) { 
						sum /= divider;
					}
					if (sum < 0) {
						targetBytes[r][c][ch] = (byte) 0;
					}
					else if (sum >= 255) {
						targetBytes[r][c][ch] = (byte) 255;
					}
					else {
						targetBytes[r][c][ch] = (byte) sum;
					}
				}
			}
		}

		Mat dst = new Mat(mat.size(), mat.type());
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					tmp[ch] = targetBytes[r][c][ch];
				}
				dst.put(r, c, tmp);
			}
		}
		return ImageUtil.matToImage(dst);
	}
	
	public BufferedImage kuwaharaFilter(BufferedImage image, int windowSize) {
		int radius = windowSize / 2;
		Mat mat = ImageUtil.imageToMat(image);

		byte[][][] matBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		byte[][][] targetBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		
		byte[] tmp = new byte[mat.channels()];
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				mat.get(r, c, tmp);
				for (int ch = 0; ch < mat.channels(); ch++) {
					matBytes[r][c][ch] = tmp[ch];
				}
			}
		}
		
		float sum, selectedSum;
		float variance, selectedVariance;
		int deFactoRow, deFactoCol, offsetRow, offsetCol;
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					selectedSum = 0;
					selectedVariance = Float.MAX_VALUE;
					for (int i = 0; i < 4; i++) { //0 = -1, -1; 1 = 1, -1; 2 = 1, 1; 3 = -1, 1
						sum = 0;
						for (int x = 0; x < radius; x++) {
							for (int y = 0; y < radius; y++) {
								offsetRow = r + y * (2 * (i % 2) - 1);
								offsetCol = c + x * (2 * (i / 2) - 1);
								deFactoRow = (offsetRow < 0) ? 0 : (offsetRow >= mat.rows() ? mat.rows() - 1 : offsetRow);
								deFactoCol = (offsetCol < 0) ? 0 : (offsetCol >= mat.cols() ? mat.cols() - 1 : offsetCol);
								sum += (int) (matBytes[deFactoRow][deFactoCol][ch] & 0xFF);
							}
						}
						sum /= (radius * radius);

						variance = 0;
						for (int x = 0; x < radius; x++) {
							for (int y = 0; y < radius; y++) {
								offsetRow = r + y * (2 * (i % 2) - 1);
								offsetCol = c + x * (2 * (i / 2) - 1);
								deFactoRow = (offsetRow < 0) ? 0 : (offsetRow >= mat.rows() ? mat.rows() - 1 : offsetRow);
								deFactoCol = (offsetCol < 0) ? 0 : (offsetCol >= mat.cols() ? mat.cols() - 1 : offsetCol);
								variance += Math.pow(((int) (matBytes[deFactoRow][deFactoCol][ch] & 0xFF)) - sum, 2);
							}
						}
						variance /= (radius * radius);
						
						if (variance < selectedVariance) {
							selectedVariance = variance;
							selectedSum = sum;
						}
					}
					if (selectedSum < 0) {
						targetBytes[r][c][ch] = (byte) 0;
					}
					else if (selectedSum >= 255) {
						targetBytes[r][c][ch] = (byte) 255;
					}
					else {
						targetBytes[r][c][ch] = (byte) selectedSum;
					}
				}
			}
		}

		Mat dst = new Mat(mat.size(), mat.type());
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					tmp[ch] = targetBytes[r][c][ch];
				}
				dst.put(r, c, tmp);
			}
		}
		return ImageUtil.matToImage(dst);
	}
	
	private class ByteComparator implements Comparator<Byte> {

		@Override
		public int compare(Byte o1, Byte o2) {
			return Integer.compare((int) (o1 & 0xFF), (int) (o2 & 0xFF));
		}
		
	}
	
	public BufferedImage medianFilter(BufferedImage image, int windowSize) {
		int matrixWidth = windowSize;
		int matrixHeight = windowSize;
		int middleX = matrixWidth / 2;
		int middleY = matrixHeight / 2;
		int medianIndex = (windowSize * windowSize) / 2;
		Mat mat = ImageUtil.imageToMat(image);

		byte[][][] matBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		byte[][][] targetBytes = new byte[mat.rows()][mat.cols()][mat.channels()];
		
		byte[] tmp = new byte[mat.channels()];
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				mat.get(r, c, tmp);
				for (int ch = 0; ch < mat.channels(); ch++) {
					matBytes[r][c][ch] = tmp[ch];
				}
			}
		}
		
		ArrayList<Byte> medianList = new ArrayList<>(windowSize * windowSize);
		Comparator<Byte> byteComparator = new ByteComparator();
		int deFactoRow, deFactoCol, offsetRow, offsetCol;
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					medianList.clear();
					for (int x = 0; x < matrixWidth; x++) {
						for (int y = 0; y < matrixHeight; y++) {
							offsetRow = r + y - middleY;
							offsetCol = c + x - middleX;
							deFactoRow = (offsetRow < 0) ? 0 : (offsetRow >= mat.rows() ? mat.rows() - 1 : offsetRow);
							deFactoCol = (offsetCol < 0) ? 0 : (offsetCol >= mat.cols() ? mat.cols() - 1 : offsetCol);
							medianList.add(matBytes[deFactoRow][deFactoCol][ch]);
						}
					}
					medianList.sort(byteComparator);
					targetBytes[r][c][ch] = medianList.get(medianIndex);
				}
			}
		}

		Mat dst = new Mat(mat.size(), mat.type());
		for (int r = 0; r < mat.rows(); r++) {
			for (int c = 0; c < mat.cols(); c++) {
				for (int ch = 0; ch < mat.channels(); ch++) {
					tmp[ch] = targetBytes[r][c][ch];
				}
				dst.put(r, c, tmp);
			}
		}
		return ImageUtil.matToImage(dst);
	}


}
