package mbud;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import mbud.event.CanvasPixelSelected;
import mbud.util.MathUtil;

public class ImageCanvas extends ImagePanel implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	private static final long serialVersionUID = -1167314879243348402L;
	
	private DoubleProperty scaleProperty = new SimpleDoubleProperty(1.0);
	private DoubleProperty translateXProperty = new SimpleDoubleProperty(0);
	private DoubleProperty translateYProperty = new SimpleDoubleProperty(0);
	
	private Dimension canvasSize = new Dimension(400, 540);
	
	private Point pointPrevFrame = null;
	private Point selectingPoint = null;
	
	private List<CanvasPixelSelected> pixelSelectedListeners = new LinkedList<>();
	
	public ImageCanvas() {
		super();
		
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
	}

	@Override
	protected void _setImage(BufferedImage image) {
		super._setImage(image);
		scaleProperty.set(canvasSize.getWidth() / image.getWidth());
		translateXProperty.set(0.0);
		translateYProperty.set(0.0);
	}
	
	public void setZoom(double zoom) {
		scaleProperty.set(zoom);
		repaint();
	}
	
	public void setTranslation(double x, double y) {
		translateXProperty.set(x);
		translateYProperty.set(y);
		repaint();
	}
	
	public void addPixelSelectedListener(CanvasPixelSelected listener) {
		pixelSelectedListeners.add(listener);
	}
	
	public DoubleProperty getScaleProperty() {
		return scaleProperty;
	}
	
	public DoubleProperty getTranslateXProperty() {
		return translateXProperty;
	}
	
	public DoubleProperty getTranslateYProperty() {
		return translateYProperty;
	}
	
	@Override
	protected void paintImage(Graphics2D g2d) {
		AffineTransform oldTranform = g2d.getTransform();
		AffineTransform transform = new AffineTransform();
		transform.scale(scaleProperty.doubleValue(), scaleProperty.doubleValue());
		transform.translate(translateXProperty.doubleValue(), translateYProperty.doubleValue());
		g2d.transform(transform);
		super.paintImage(g2d);
		g2d.setTransform(oldTranform);
	}

	@Override
	public Dimension getPreferredSize() {
		return canvasSize;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (!hasLoadedImage()) {
			return;
		}
		if (pointPrevFrame != null) {
			translateXProperty.set(MathUtil.clamp(translateXProperty.doubleValue() + ((e.getX() - pointPrevFrame.getX()) / scaleProperty.doubleValue()), Math.min(-getImage().getWidth() + canvasSize.getWidth() / scaleProperty.doubleValue(), 0.0), 0.0));
			translateYProperty.set(MathUtil.clamp(translateYProperty.doubleValue() + ((e.getY() - pointPrevFrame.getY()) / scaleProperty.doubleValue()), Math.min(-getImage().getHeight() + canvasSize.getHeight() / scaleProperty.doubleValue(), 0.0), 0.0));
			pointPrevFrame = e.getPoint();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if (!hasLoadedImage()) {
			return;
		}
		if (e.getButton() == MouseEvent.BUTTON3) {
			pointPrevFrame = e.getPoint();
			startRedrawing();
		}
		else if (e.getButton() == MouseEvent.BUTTON1) {
			selectingPoint = new Point(
					(int) (e.getPoint().getX() / scaleProperty.doubleValue() - translateXProperty.doubleValue()),
					(int) (e.getPoint().getY() / scaleProperty.doubleValue() - translateYProperty.doubleValue()));
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (!hasLoadedImage()) {
			return;
		}
		pointPrevFrame = null;
		if (selectingPoint != null) {
			if (selectingPoint.getX() >= 0 && selectingPoint.getX() <= getImage().getWidth() &&
					selectingPoint.getY() >= 0 && selectingPoint.getY() <= getImage().getHeight()) {
				for (CanvasPixelSelected l : pixelSelectedListeners) {
					l.onPixelSelected(selectingPoint);
				}
			}
			selectingPoint = null;
		}
		stopRedrawing();
		repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (!hasLoadedImage()) {
			return;
		}
		scaleProperty.set(scaleProperty.doubleValue() * Math.pow(1.02, e.getWheelRotation()));
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
