package mbud;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.opencv.core.CvException;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import mbud.filefilter.CustomFileFilter;
import mbud.image.CustomImageIO;
import mbud.image.ImageProcessor;
import mbud.image.ImageUtil;
import mbud.image.IncorrectParametersException;
import mbud.util.InstantChangeNumberSpinner;
import mbud.util.RegexFormatter;

public class MainPanel extends JPanel {

	private static final long serialVersionUID = 8885550223477187087L;

	private class ToolPanel extends JPanel {
		private static final long serialVersionUID = 6584634990065144607L;

		private JSlider red, green, blue;
		private JFormattedTextField redField, greenField, blueField;
		private String regexForFields = "^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$";
		private Point selected = null;
		private Boolean settingInitialValues = false;

		public ToolPanel() {
			setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

			JPanel redPanel = new JPanel(new FlowLayout());
			JLabel redLabel = new JLabel("Czerwony");
			redLabel.setPreferredSize(new Dimension(80, 20));
			redPanel.add(redLabel);
			red = new JSlider(SwingConstants.HORIZONTAL, 0, 255, 128);
			red.setPreferredSize(new Dimension(100, 20));
			red.addChangeListener(this::slidersChanged);
			redPanel.add(red);
			redField = new JFormattedTextField(new RegexFormatter(regexForFields));
			redField.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					red.setValue(Integer.parseInt(redField.getText()));

				}
			});
			redField.setPreferredSize(new Dimension(30, 20));
			redPanel.add(redField);

			JPanel greenPanel = new JPanel(new FlowLayout());
			JLabel greenLabel = new JLabel("Zielony");
			greenLabel.setPreferredSize(new Dimension(80, 20));
			greenPanel.add(greenLabel);
			green = new JSlider(SwingConstants.HORIZONTAL, 0, 255, 128);
			green.setPreferredSize(new Dimension(100, 20));
			green.addChangeListener(this::slidersChanged);
			greenPanel.add(green);
			greenField = new JFormattedTextField(new RegexFormatter(regexForFields));
			greenField.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					green.setValue(Integer.parseInt(greenField.getText()));

				}
			});
			greenField.setPreferredSize(new Dimension(30, 20));
			greenPanel.add(greenField);

			JPanel bluePanel = new JPanel(new FlowLayout());
			JLabel blueLabel = new JLabel("Niebieski");
			blueLabel.setPreferredSize(new Dimension(80, 20));
			bluePanel.add(blueLabel);
			blue = new JSlider(SwingConstants.HORIZONTAL, 0, 255, 128);
			blue.setPreferredSize(new Dimension(100, 20));
			blue.addChangeListener(this::slidersChanged);
			bluePanel.add(blue);
			blueField = new JFormattedTextField(new RegexFormatter(regexForFields));
			blueField.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					blue.setValue(Integer.parseInt(blueField.getText()));

				}
			});
			blueField.setPreferredSize(new Dimension(30, 20));
			bluePanel.add(blueField);

			add(redPanel);
			add(greenPanel);
			add(bluePanel);
		}

		public void onPixelSelected(Point pixel) {
			selected = pixel;
			Color colors = new Color(editingCanvas.getImage().getRGB((int) pixel.getX(), (int) pixel.getY()));
			settingInitialValues = true;
			red.setValue(colors.getRed());
			green.setValue(colors.getGreen());
			blue.setValue(colors.getBlue());
			if (redrawTimer.isRunning()) {
				redrawTimer.stop();
				createHistograms();
			}
			settingInitialValues = false;
		}

		public void slidersChanged(ChangeEvent e) {
			if (selected == null) {
				return;
			}
			Color color = new Color(red.getValue(), green.getValue(), blue.getValue());
			editingCanvas.getImage().setRGB((int) selected.getX(), (int) selected.getY(), color.getRGB());
			editingCanvas.repaint();
			redField.setText("" + red.getValue());
			greenField.setText("" + green.getValue());
			blueField.setText("" + blue.getValue());
			if (!settingInitialValues) {
				redrawTimer.start();
			}
		}

		@Override
		public Dimension getPreferredSize() {
			// TODO Auto-generated method stub
			return new Dimension(240, 400);
		}
	}

	private File currentFile;
	private ImageCanvas editingCanvas, originalCanvas;
	private ImagePanel[] histogramPanels;
	private JLabel infoPanel;
	private ToolPanel toolPanel;
	private Timer redrawTimer = new Timer(5000, null);
	private ImageProcessor imageProcessor = new ImageProcessor();
	
	private int[][] linearFilterValues = {
			{ 0, 0, 0 },
			{ 0, 1, 0 },
			{ 0, 0, 0}
	};

	public MainPanel() {
		BorderLayout mainLayout = new BorderLayout();
		setLayout(mainLayout);

		JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("Plik");
		JMenuItem openFileItem = new JMenuItem("Otwórz");
		openFileItem.addActionListener(this::loadFile);
		JMenuItem saveFileItem = new JMenuItem("Zapisz");
		saveFileItem.addActionListener(this::saveFile);
		JMenuItem saveAsFileItem = new JMenuItem("Zapisz jako");
		saveAsFileItem.addActionListener(this::saveAsFile);
		JMenuItem closeItem = new JMenuItem("Zamknij");
		closeItem.addActionListener(this::closeApplication);
		fileMenu.add(openFileItem);
		fileMenu.add(saveFileItem);
		fileMenu.add(saveAsFileItem);
		fileMenu.add(closeItem);
		menuBar.add(fileMenu);

		JMenu editMenu = new JMenu("Edycja");
		JMenuItem scaleItem = new JMenuItem("Skaluj obraz");
		scaleItem.addActionListener(this::scaleImage);
		editMenu.add(scaleItem);
		JMenuItem restoreItem = new JMenuItem("Przywróć oryginał");
		restoreItem.addActionListener(this::restoreOriginal);
		editMenu.add(restoreItem);
		menuBar.add(editMenu);

		JMenu viewMenu = new JMenu("Widok");
		JMenuItem adjustZoomItem = new JMenuItem("Dopasuj zbliżenie do obrazu");
		adjustZoomItem.addActionListener(
				e -> setZoom(editingCanvas.getPreferredSize().getWidth() / editingCanvas.getImage().getWidth()));
		viewMenu.add(adjustZoomItem);
		JMenuItem defaultZoomItem = new JMenuItem("Domyślnie zbliżenie (100%)");
		defaultZoomItem.addActionListener(e -> setZoom(1.0));
		viewMenu.add(defaultZoomItem);
		JMenuItem zoom200Item = new JMenuItem("Zbliżenie 200%");
		zoom200Item.addActionListener(e -> setZoom(2.0));
		viewMenu.add(zoom200Item);
		JMenuItem zoom400Item = new JMenuItem("Zbliżenie 400%");
		zoom400Item.addActionListener(e -> setZoom(4.0));
		viewMenu.add(zoom400Item);
		JMenuItem zoom800Item = new JMenuItem("Zbliżenie 800%");
		zoom800Item.addActionListener(e -> setZoom(8.0));
		viewMenu.add(zoom800Item);
		JMenuItem defaultTranslationItem = new JMenuItem("Domyślne przesunięcie");
		defaultTranslationItem.addActionListener(e -> setTranslation(0, 0));
		viewMenu.add(defaultTranslationItem);
		menuBar.add(viewMenu);

		JMenu colorMenu = new JMenu("Kolor");
		JMenuItem grayScaleItem = new JMenuItem("Uśrednienie kolorów");
		grayScaleItem.addActionListener(e -> {
			editingCanvas.setImage(imageProcessor.turnToGrayScale(editingCanvas.getImage()));
			createHistograms();
		});
		colorMenu.add(grayScaleItem);
		JMenuItem brightenItem = new JMenuItem("Rozjaśnienie wykładnicze");
		brightenItem.addActionListener(this::brightenImage);
		colorMenu.add(brightenItem);
		JMenuItem darkenItem = new JMenuItem("Przyciemnienie wykładnicze");
		darkenItem.addActionListener(this::darkenImage);
		colorMenu.add(darkenItem);
		JMenuItem stretchItem = new JMenuItem("Rozciągnij histogram");
		stretchItem.addActionListener(this::stretchHistogram);
		colorMenu.add(stretchItem);
		JMenuItem equilizeItem = new JMenuItem("Wyrównaj histogram");
		equilizeItem.addActionListener(e -> {
			editingCanvas.setImage(imageProcessor.equilizeHistogram(editingCanvas.getImage()));
			createHistograms();
		});
		colorMenu.add(equilizeItem);
		menuBar.add(colorMenu);

		JMenu binarizationMenu = new JMenu("Binaryzacja");
		JMenuItem manualBinarization = new JMenuItem("Binaryzacja ręczna");
		manualBinarization.addActionListener(this::binarizeThreshold);
		binarizationMenu.add(manualBinarization);
		JMenuItem binarizationOtsu = new JMenuItem("Binaryzacja Otsu");
		binarizationOtsu.addActionListener(e -> {
			editingCanvas.setImage(imageProcessor.binarizeOtsu(editingCanvas.getImage()));
			createHistograms();
		});
		binarizationMenu.add(binarizationOtsu);
		JMenuItem binarizeNiblack = new JMenuItem("Binaryzacja Niblack");
		binarizeNiblack.addActionListener(this::binarizeNiblack);
		binarizationMenu.add(binarizeNiblack);
		menuBar.add(binarizationMenu);
		
		JMenu filterMenu = new JMenu("Filtry");
		JMenuItem filterLinear = new JMenuItem("Liniowe");
		filterLinear.addActionListener(this::linearFilter);
		filterMenu.add(filterLinear);
		JMenuItem filterKuwahara = new JMenuItem("Kuwahara");
		filterKuwahara.addActionListener(e -> {
			editingCanvas.setImage(imageProcessor.kuwaharaFilter(editingCanvas.getImage(), 5));
			createHistograms();
		});
		filterMenu.add(filterKuwahara);
		JMenuItem filterMedian = new JMenuItem("Mediana");
		filterMedian.addActionListener(this::medianFilter);
		filterMenu.add(filterMedian);
		menuBar.add(filterMenu);

		add(menuBar, BorderLayout.NORTH);

		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.LINE_AXIS));
		originalCanvas = new ImageCanvas();
		originalCanvas.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));
		centerPanel.add(originalCanvas);
		editingCanvas = new ImageCanvas();
		editingCanvas.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));
		centerPanel.add(editingCanvas);
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new GridLayout(4, 1));
		histogramPanels = new ImagePanel[4];
		for (int i = 0; i < 4; i++) {
			histogramPanels[i] = new ImagePanel();
			histogramPanels[i].setPreferredSize(new Dimension(266, 135));
			if (i != 3) {
				histogramPanels[i].setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY));
			}
			leftPanel.add(histogramPanels[i]);
		}
		centerPanel.add(leftPanel);

		add(centerPanel, BorderLayout.CENTER);

		infoPanel = new JLabel(" ");
		infoPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));

		add(infoPanel, BorderLayout.SOUTH);

		toolPanel = new ToolPanel();
		toolPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));

		add(toolPanel, BorderLayout.EAST);

		originalCanvas.getScaleProperty().addListener(this::onZoomChanged);
		editingCanvas.getScaleProperty().addListener(this::onZoomChanged);
		editingCanvas.addPixelSelectedListener(this::onPixelSelected);
		editingCanvas.addPixelSelectedListener(toolPanel::onPixelSelected);
		redrawTimer.addActionListener(e -> {
			createHistograms();
			redrawTimer.stop();
		});
	}

	private void brightenImage(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		String s = (String) JOptionPane.showInputDialog(MainWindow.getInstance(),
				"Podaj wartość transformacji (liczba dziesiętna większa od 0):", "Rozjaśnianie",
				JOptionPane.PLAIN_MESSAGE, null, null, "1");

		if (s != null && s.length() > 0) {
			Pattern pattern = Pattern.compile("[+]?[0-9]*[\\.,]?[0-9]+");
			Matcher matcher = pattern.matcher(s);
			if (matcher.matches()) {
				double val;
				try {
					val = Double.parseDouble(s);
				} catch (NumberFormatException ex) {
					s = s.replace(',', '.');
					try {
						val = Double.parseDouble(s);
					} catch (NumberFormatException ex2) {
						infoPanel.setText("Podana wartość nie jest liczbą!");
						return;
					}
				}
				if (val < 0.0) {
					infoPanel.setText("Podana wartość jest nieprawidłowa!");
					return;
				}
				editingCanvas.setImage(imageProcessor.brighten(editingCanvas.getImage(), 1 + val));
				createHistograms();
				return;
			} else {
				infoPanel.setText("Podana wartość jest nieprawidłowa!");
			}
			return;
		}
		infoPanel.setText("Wymagane jest podanie wartości!");
	}

	private void darkenImage(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		String s = (String) JOptionPane.showInputDialog(MainWindow.getInstance(),
				"Podaj wartość transformacji (liczba dziesiętna większa od 0):", "Rozjaśnianie",
				JOptionPane.PLAIN_MESSAGE, null, null, "1");

		if (s != null && s.length() > 0) {
			Pattern pattern = Pattern.compile("[+]?[0-9]*[\\.,]?[0-9]+");
			Matcher matcher = pattern.matcher(s);
			if (matcher.matches()) {
				double val;
				try {
					val = Double.parseDouble(s);
				} catch (NumberFormatException ex) {
					s = s.replace(',', '.');
					try {
						val = Double.parseDouble(s);
					} catch (NumberFormatException ex2) {
						infoPanel.setText("Podana wartość nie jest liczbą!");
						return;
					}
				}
				if (val < 0.0) {
					infoPanel.setText("Podana wartość jest nieprawidłowa!");
					return;
				}
				editingCanvas.setImage(imageProcessor.darken(editingCanvas.getImage(), 1 + val));
				createHistograms();
				return;
			} else {
				infoPanel.setText("Podana wartość jest nieprawidłowa!");
			}
			return;
		}
		infoPanel.setText("Wymagane jest podanie wartości!");
	}

	private void binarizeThreshold(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		String s = (String) JOptionPane.showInputDialog(MainWindow.getInstance(), "Podaj próg binaryzacji:",
				"Binaryzacja ręczna", JOptionPane.PLAIN_MESSAGE, null, null, "127");

		if (s != null && s.length() > 0) {
			Pattern pattern = Pattern.compile("[+]?[0-9]*[0-9]+");
			Matcher matcher = pattern.matcher(s);
			if (matcher.matches()) {
				int val;
				try {
					val = Integer.parseInt(s);
				} catch (NumberFormatException ex) {
					infoPanel.setText("Podana wartość nie jest liczbą!");
					return;
				}
				if (val < 0 || val > 255) {
					infoPanel.setText("Podana wartość jest nieprawidłowa!");
					return;
				}
				editingCanvas.setImage(imageProcessor.binarize(editingCanvas.getImage(), 1 + val));
				createHistograms();
				return;
			} else {
				infoPanel.setText("Podana wartość jest nieprawidłowa!");
			}
			return;
		}
		infoPanel.setText("Wymagane jest podanie wartości!");
	}

	private void binarizeNiblack(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		JDialog frame = new JDialog(MainWindow.getInstance(), "Binaryzacja Niblack", false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		InstantChangeNumberSpinner boxSizeSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(15, 1, 99999, 2));
		InstantChangeNumberSpinner parameterSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(-0.2, -10000.0, 0.0, 0.001));

		JButton acceptButton = new JButton("Binaryzuj");
		acceptButton.addActionListener(ev -> {
			int boxSize = (Integer) boxSizeSpinner.getValue();
			if (boxSize <= 1 || boxSize % 2 != 1) {
				infoPanel.setText("Rozmiar okna musi być wartością większą od 1 i podzielną przez 2!");
				return;
			}
			try {
				editingCanvas.setImage(imageProcessor.binarizeNiblack(editingCanvas.getImage(),
						boxSize, Float.parseFloat(parameterSpinner.getValue().toString())));
			} catch (NumberFormatException|IncorrectParametersException e1) {
				infoPanel.setText("Podano niepoprawne parametry!");
			}
			createHistograms();
			frame.setVisible(false);
		});
		JButton cancelButton = new JButton("Anuluj");
		cancelButton.addActionListener(ev -> frame.setVisible(false));

		panel.add(new JLabel("Podaj rozmiar okna oraz parametr binaryzowania:"), c);
		JPanel scalePanel = new JPanel();
		scalePanel.add(boxSizeSpinner);
		scalePanel.add(parameterSpinner);
		c.gridy = 1;
		panel.add(scalePanel, c);
		panel.add(scalePanel, c);

		c.gridy = 2;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(acceptButton);
		buttonPanel.add(cancelButton);
		panel.add(buttonPanel, c);

		frame.setAlwaysOnTop(true);
		frame.setModalityType(ModalityType.APPLICATION_MODAL);
		frame.setContentPane(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private void stretchHistogram(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		JDialog frame = new JDialog(MainWindow.getInstance(), "Skalowanie obrazu", false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		InstantChangeNumberSpinner stretchStartSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(0, 0, 255, 1));
		InstantChangeNumberSpinner stretchStopSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(255, 0, 255, 1));

		JButton acceptButton = new JButton("Rozciągnij");
		acceptButton.addActionListener(ev -> {
			editingCanvas.setImage(imageProcessor.stretchHistogram(editingCanvas.getImage(),
					(Integer) stretchStartSpinner.getValue(), (Integer) stretchStopSpinner.getValue()));
			createHistograms();
			frame.setVisible(false);
		});
		JButton cancelButton = new JButton("Anuluj");
		cancelButton.addActionListener(ev -> frame.setVisible(false));

		panel.add(new JLabel("Podaj ograniczenia zakresu jasności (0-255), jaki zostanie rozciągnięty:"), c);
		JPanel scalePanel = new JPanel();
		scalePanel.add(stretchStartSpinner);
		scalePanel.add(stretchStopSpinner);
		c.gridy = 1;
		panel.add(scalePanel, c);

		c.gridy = 2;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(acceptButton);
		buttonPanel.add(cancelButton);
		panel.add(buttonPanel, c);

		frame.setAlwaysOnTop(true);
		frame.setModalityType(ModalityType.APPLICATION_MODAL);
		frame.setContentPane(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	private void scaleImage(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}
		JDialog frame = new JDialog(MainWindow.getInstance(), "Skalowanie obrazu", false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		JLabel sizeLabel = new JLabel("Aktualny rozmiar: " + editingCanvas.getImage().getWidth() + "x"
				+ editingCanvas.getImage().getHeight());
		sizeLabel.setAlignmentX(LEFT_ALIGNMENT);
		panel.add(sizeLabel, c);
		InstantChangeNumberSpinner scaleXSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(1.0, 0.001, 9999.999, 0.001));
		InstantChangeNumberSpinner scaleYSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(1.0, 0.001, 9999.999, 0.001));

		InstantChangeNumberSpinner widthSpinner = new InstantChangeNumberSpinner(new SpinnerNumberModel(
				editingCanvas.getImage().getWidth(), 1, editingCanvas.getImage().getWidth() * 10000 - 1, 1));
		InstantChangeNumberSpinner heightSpinner = new InstantChangeNumberSpinner(new SpinnerNumberModel(
				editingCanvas.getImage().getHeight(), 1, editingCanvas.getImage().getHeight() * 10000 - 1, 1));

		final BooleanProperty canWidthUpdate = new SimpleBooleanProperty(true),
				canHeightUpdate = new SimpleBooleanProperty(true);
		scaleXSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!canWidthUpdate.get()) {
					return;
				}
				canWidthUpdate.set(false);
				widthSpinner.setValue((int) (editingCanvas.getImage().getWidth()
						* Double.parseDouble(scaleXSpinner.getValue().toString())));
				canWidthUpdate.set(true);
			}
		});
		scaleYSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!canHeightUpdate.get()) {
					return;
				}
				canHeightUpdate.set(false);
				heightSpinner.setValue((int) (editingCanvas.getImage().getHeight()
						* Double.parseDouble(scaleYSpinner.getValue().toString())));
				canHeightUpdate.set(true);
			}
		});
		widthSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!canWidthUpdate.get()) {
					return;
				}
				canWidthUpdate.set(false);
				scaleXSpinner.setValue(
						Double.parseDouble(widthSpinner.getValue().toString()) / editingCanvas.getImage().getWidth());
				canWidthUpdate.set(true);
			}
		});
		heightSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!canHeightUpdate.get()) {
					return;
				}
				canHeightUpdate.set(false);
				scaleYSpinner.setValue(
						Double.parseDouble(heightSpinner.getValue().toString()) / editingCanvas.getImage().getHeight());
				canHeightUpdate.set(true);
			}
		});

		JButton acceptButton = new JButton("Skaluj");
		acceptButton.addActionListener(ev -> {
			editingCanvas.setImage(ImageUtil.scaleImage(editingCanvas.getImage(), (int) widthSpinner.getValue(),
					(int) heightSpinner.getValue()));
			createHistograms();
			frame.setVisible(false);
		});
		JButton cancelButton = new JButton("Anuluj");
		cancelButton.addActionListener(ev -> frame.setVisible(false));

		c.gridy = 1;
		panel.add(new JLabel("Zmiana skali:"), c);
		JPanel scalePanel = new JPanel();
		scalePanel.add(scaleXSpinner);
		scalePanel.add(scaleYSpinner);
		c.gridy = 2;
		panel.add(scalePanel, c);
		c.gridy = 3;
		panel.add(new JLabel("Zmiana rozmiaru:"), c);
		JPanel sizePanel = new JPanel();
		sizePanel.add(widthSpinner);
		sizePanel.add(heightSpinner);
		c.gridy = 4;
		panel.add(sizePanel, c);

		c.gridy = 5;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(acceptButton);
		buttonPanel.add(cancelButton);
		panel.add(buttonPanel, c);

		frame.setAlwaysOnTop(true);
		frame.setModalityType(ModalityType.APPLICATION_MODAL);
		frame.setContentPane(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private void linearFilter(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}
		JDialog frame = new JDialog(MainWindow.getInstance(), "Liniowy filtr", false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(new JLabel("Podaj wartości macierzy filtra liniowego:"));
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel matrixPanel = new JPanel(new GridBagLayout());
		InstantChangeNumberSpinner[][] spinners = new InstantChangeNumberSpinner[3][3];
		for (int i = 0; i < 3; i++) {
			c.gridx = i;
			for (int j = 0; j < 3; j++) {
				c.gridy = j;
				spinners[i][j] = new InstantChangeNumberSpinner(new SpinnerNumberModel(linearFilterValues[i][j], -1000, 1000, 1));
				matrixPanel.add(spinners[i][j], c);
			}
		}
		c.gridx = 0;
		c.gridy = 1;
		panel.add(matrixPanel, c);

		JButton acceptButton = new JButton("Filtr");
		acceptButton.addActionListener(ev -> {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					linearFilterValues[i][j] = (int) spinners[i][j].getValue();
				}
			}
			editingCanvas.setImage(imageProcessor.linearFilter(editingCanvas.getImage(), 
					new int[][] {
				{ linearFilterValues[0][0], linearFilterValues[0][1], linearFilterValues[0][2] },
				{ linearFilterValues[1][0], linearFilterValues[1][1], linearFilterValues[1][2] },
				{ linearFilterValues[2][0], linearFilterValues[2][1], linearFilterValues[2][2] }
			}));
			createHistograms();
			frame.setVisible(false);
		});
		JButton cancelButton = new JButton("Anuluj");
		cancelButton.addActionListener(ev -> frame.setVisible(false));

		c.gridy = 5;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(acceptButton);
		buttonPanel.add(cancelButton);
		panel.add(buttonPanel, c);

		frame.setAlwaysOnTop(true);
		frame.setModalityType(ModalityType.APPLICATION_MODAL);
		frame.setContentPane(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private void medianFilter(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}

		JDialog frame = new JDialog(MainWindow.getInstance(), "Filtr medianowy", false);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		InstantChangeNumberSpinner boxSizeSpinner = new InstantChangeNumberSpinner(
				new SpinnerNumberModel(3, 3, 99999, 2));

		JButton acceptButton = new JButton("Filtr");
		acceptButton.addActionListener(ev -> {
			editingCanvas.setImage(imageProcessor.medianFilter(editingCanvas.getImage(), (int) boxSizeSpinner.getValue())); 
			createHistograms();
			frame.setVisible(false);
		});
		JButton cancelButton = new JButton("Anuluj");
		cancelButton.addActionListener(ev -> frame.setVisible(false));

		panel.add(new JLabel("Podaj rozmiar okna:"), c);
		JPanel scalePanel = new JPanel();
		scalePanel.add(boxSizeSpinner);
		c.gridy = 1;
		panel.add(scalePanel, c);
		panel.add(scalePanel, c);

		c.gridy = 2;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
		buttonPanel.add(acceptButton);
		buttonPanel.add(cancelButton);
		panel.add(buttonPanel, c);

		frame.setAlwaysOnTop(true);
		frame.setModalityType(ModalityType.APPLICATION_MODAL);
		frame.setContentPane(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}


	private void setZoom(double zoomValue) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}
		editingCanvas.setZoom(zoomValue);
		originalCanvas.setZoom(zoomValue);
	}

	private void setTranslation(double x, double y) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}
		editingCanvas.setTranslation(x, y);
		originalCanvas.setTranslation(x, y);
	}

	private void onZoomChanged(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		infoPanel.setText("Zbliżenie: " + Math.round(100 * newValue.doubleValue()) + "%");
	}

	private void onPixelSelected(Point pixel) {
		infoPanel.setText("Wybrano piksel (" + (int) pixel.getX() + ", " + (int) pixel.getY() + ")");
	}

	private void restoreOriginal(ActionEvent e) {
		if (originalCanvas.getImage() == null) {
			infoPanel.setText("Nie wczytano obrazu!");
			return;
		}
		editingCanvas.setImage(ImageUtil.copy(originalCanvas.getImage()));
		createHistograms();
	}

	private void createHistograms() {
		BufferedImage[] histograms = imageProcessor.generateHistogram(editingCanvas.getImage(),
				new Dimension(256, 125));
		for (int i = 0; i < 4; i++) {
			histogramPanels[i].setImage(histograms[i]);
		}
	}

	private void loadFile(ActionEvent e) {
		JFileChooser fileChooser = new JFileChooser(new File("."));
		CustomFileFilter fileFilter = new CustomFileFilter("bmp", "gif", "jpeg", "jpg", "png", "tif", "tiff");
		fileFilter.setDescription("Obraz w dowolnym formacie");
		fileChooser.setFileFilter(fileFilter);

		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			try {
				BufferedImage image = CustomImageIO.loadImage(selectedFile);
				editingCanvas.setImage(image);
				originalCanvas.setImage(ImageUtil.copy(image));
				createHistograms();
				currentFile = selectedFile;
			} catch (CvException ex) {
				ex.printStackTrace(System.err);
				JOptionPane.showMessageDialog(getParent(), "Błąd wczytywania pliku!", "Błąd!",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void saveFile(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie można zapisać obrazu, który nie istnieje!");
			return;
		}
		try {
			CustomImageIO.saveImage(editingCanvas.getImage(), currentFile);
			infoPanel.setText("Zapisano!");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			JOptionPane.showMessageDialog(getParent(), "Błąd zapisywania pliku!", "Błąd!", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void saveAsFile(ActionEvent e) {
		if (editingCanvas.getImage() == null) {
			infoPanel.setText("Nie można zapisać obrazu, który nie istnieje!");
			return;
		}
		JFileChooser fileChooser = new JFileChooser(new File("."));
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.addChoosableFileFilter(new CustomFileFilter("bmp"));
		fileChooser.addChoosableFileFilter(new CustomFileFilter("gif"));
		fileChooser.addChoosableFileFilter(new CustomFileFilter("jpeg", "jpg"));
		fileChooser.addChoosableFileFilter(new CustomFileFilter("png"));
		fileChooser.addChoosableFileFilter(new CustomFileFilter("tif", "tiff"));

		int returnVal = fileChooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			CustomFileFilter filter = (CustomFileFilter) fileChooser.getFileFilter();
			try {
				CustomImageIO.saveImage(editingCanvas.getImage(), filter.correctFileExtension(selectedFile));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
				JOptionPane.showMessageDialog(getParent(), "Błąd wczytywania pliku!", "Błąd!",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void closeApplication(ActionEvent e) {
		MainWindow.getInstance().dispatchEvent(new WindowEvent(MainWindow.getInstance(), WindowEvent.WINDOW_CLOSING));
	}
}
