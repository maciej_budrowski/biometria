package mbud.filefilter;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class CustomFileFilter extends FileFilter {

	private String[] extensions;
	private String customDesc;

	public CustomFileFilter(String... extensions) {
		this.extensions = extensions;
		for (int i = 0; i < extensions.length; i++) {
			this.extensions[i] = this.extensions[i].toUpperCase();
		}
	}
	
	public void setDescription(String desc) {
		this.customDesc = desc;
	}
	
	private boolean canBeDisplayed(File file) {
		try {
		    String extension = file.getName().split("\\.(?=[^\\.]+$)")[1].toUpperCase();
		    if (extension != null) {
		    	for (String e : extensions) {
		    		if (e.equals(extension)) {
		    			return true;
		    		}
		    	}
	    		return false;
		    }
			else {
				return true;
			}
		}
		catch (ArrayIndexOutOfBoundsException e) {
			return true;
		}
	}
	
	private boolean needsExtension(File file) {
		try {
		    String extension = file.getName().split("\\.(?=[^\\.]+$)")[1].toUpperCase();
		    if (extension != null) {
		    	for (String e : extensions) {
		    		if (e.equals(extension)) {
		    			return false;
		    		}
		    	}
		    }
			return true;
		}
		catch (ArrayIndexOutOfBoundsException e) {
			return true;
		}
	}
	
	public File correctFileExtension(File file) {
		if (needsExtension(file)) {
			return new File(file.getPath() + "." + extensions[0]);
		}
		return file;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder();
		if (customDesc == null) {
			sb.append("Obraz ");
			sb.append(extensions[0]);
		}
		else {
			sb.append(customDesc);
		}
		sb.append(" (*." + extensions[0]);
		for (int i = 1; i < extensions.length; i++) {
			sb.append(", *." + extensions[i]);
		}
		sb.append(")");
		return sb.toString();
	}

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		return canBeDisplayed(f);
	}
}