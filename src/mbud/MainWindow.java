package mbud;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.opencv.core.Core;

public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = 2837944835331679668L;
	private static MainWindow instance;
	
	public static MainWindow getInstance() {
		if (instance == null) {
			instance = new MainWindow();
		}
		return instance;
	}
	
	public MainWindow() {
		setTitle("Biometria");
		setContentPane(new MainPanel());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setResizable(false);
	}
	
	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				JFrame mainWindow = MainWindow.getInstance();
				mainWindow.setLocationRelativeTo(null);
				mainWindow.setVisible(true);
			}
		});
	}
}
