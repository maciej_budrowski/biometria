package mbud.event;

import java.awt.Point;

public interface CanvasPixelSelected {
	void onPixelSelected(Point pixel);
}
