package mbud;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.Timer;

public class ImagePanel extends JPanel {

	private static final long serialVersionUID = -7088384564080362570L;
	
	private BufferedImage image;
	private Timer redrawTimer = new Timer(20, e -> repaint());
	
	public ImagePanel() {
		super();
	}

	public void setImage(BufferedImage image) {
		_setImage(image);
		repaint();
	}
	
	protected void _setImage(BufferedImage image) {
		this.image = image;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	protected void startRedrawing() {
		redrawTimer.restart();
	}
	
	protected void stopRedrawing() {
		redrawTimer.stop();
	}
	
	protected boolean hasLoadedImage() {
		return image != null;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image == null) {
			return;
		}
		paintImage((Graphics2D) g);
	}
	
	protected void paintImage(Graphics2D g2d) {
		g2d.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), 0, 0, image.getWidth(), image.getHeight(), null);
	}
}
