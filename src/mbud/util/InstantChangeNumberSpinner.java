package mbud.util;

import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatter;

public class InstantChangeNumberSpinner extends JSpinner {
	private static final long serialVersionUID = 4273481544306967314L;

	public InstantChangeNumberSpinner(SpinnerNumberModel model) {
		super(model);

		JComponent comp = getEditor();
	    JFormattedTextField field = (JFormattedTextField) comp.getComponent(0);
	    DefaultFormatter formatter = (DefaultFormatter) field.getFormatter();
	    formatter.setCommitsOnValidEdit(true);
	    
	    Dimension d = field.getPreferredSize();
	    d.width = 100;
	    field.setPreferredSize(d);
	    field.setColumns(8);
	}
}
