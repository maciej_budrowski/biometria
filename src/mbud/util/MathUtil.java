package mbud.util;

public class MathUtil {
	public static <T extends Comparable<T>> T clamp(T value, T min, T max) {
		if (value.compareTo(min) <= 0) {
			return min;
		}
		if (value.compareTo(max) >= 0) {
			return max;
		}
		return value;
	}
	
	public static double logAny(double base, double n) {
		return (Math.log(n) / Math.log(base));
	}
}
